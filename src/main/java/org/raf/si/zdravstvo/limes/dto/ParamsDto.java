package org.raf.si.zdravstvo.limes.dto;

import java.util.LinkedHashMap;

public class ParamsDto {

    private LinkedHashMap<String, Object> parameterMap;

    public ParamsDto() {
    }

    public LinkedHashMap<String, Object> getParameterMap() {
        return parameterMap;
    }

    public void setParameterMap(LinkedHashMap<String, Object> parameterMap) {
        this.parameterMap = parameterMap;
    }

    public Object getParameter(String key) {
        if (parameterMap == null) {
            parameterMap = new LinkedHashMap<>();
        }
        return parameterMap.get(key);
    }

    public void setParameter(String key, Object value) {
        if (parameterMap == null) {
            parameterMap = new LinkedHashMap<>();
        }
        parameterMap.put(key, value);
    }

    @Override
    public String toString() {
        return "ParamsDto{" +
                "parameterMap=" + parameterMap +
                '}';
    }
}

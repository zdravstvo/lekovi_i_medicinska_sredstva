package org.raf.si.zdravstvo.limes.dao;

import org.raf.si.zdravstvo.limes.dao.repositories.Crud;
import org.raf.si.zdravstvo.limes.dao.repositories.MedicineManufacturerRepository;
import org.raf.si.zdravstvo.limes.dto.MedicineManufacturerDto;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.keys.LekoviProizvodjacPK;
import org.raf.si.zdravstvo.limes.entites.many2many.LekoviProizvodjac;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MedicineManufacturerDao implements Crud<LekoviProizvodjac, LekoviProizvodjacPK> {

    @Autowired
    MedicineManufacturerRepository medicineManufacturerRepository;

    @Autowired
    CompanyDao companyDao;

    @Autowired
    MedicineDao medicineDao;

    @Override
    public Iterable<LekoviProizvodjac> getAll() {
        return medicineManufacturerRepository.findAll();
    }

    public List<MedicineManufacturerDto> smartGetAll() {
        List<MedicineManufacturerDto> smartManufacturers = new ArrayList<>();
        Iterable<LekoviProizvodjac> clients = getAll();
        String companyName, medicineName;
        for(LekoviProizvodjac k : clients) {
            companyName = companyDao.getById(k.getKompanijaId()).getKompanijaNaziv();
            medicineName = medicineDao.getById(k.getLekId()).getLekNaziv();
            smartManufacturers.add(new MedicineManufacturerDto(companyName, medicineName));
        }
        return smartManufacturers;
    }

    @Override
    public LekoviProizvodjac getById(LekoviProizvodjacPK id) {
        return medicineManufacturerRepository.findOne(id);
    }

    public MedicineManufacturerDto smartGetById(LekoviProizvodjacPK id) {
        LekoviProizvodjac k = getById(id);
        String companyName = companyDao.getById(k.getKompanijaId()).getKompanijaNaziv();
        String medicineName = medicineDao.getById(k.getLekId()).getLekNaziv();
        return new MedicineManufacturerDto(companyName, medicineName);
    }

    @Override
    public void add(LekoviProizvodjac proizvodjac) {
        medicineManufacturerRepository.save(proizvodjac);
    }

    @Override
    public void delete(LekoviProizvodjacPK id) {
        medicineManufacturerRepository.delete(id);
    }

    @Override
    public Iterable<LekoviProizvodjac> paramSearch(ParamsDto paramsDto) {
        return null;
    }

}

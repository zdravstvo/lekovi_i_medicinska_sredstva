package org.raf.si.zdravstvo.limes.dao;

import org.apache.commons.lang3.StringUtils;
import org.raf.si.zdravstvo.limes.dao.repositories.Crud;
import org.raf.si.zdravstvo.limes.dao.repositories.MedicineRepository;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.Lekovi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Component
public class MedicineDao implements Crud<Lekovi, Integer> {

    @Autowired
    MedicineRepository medicineRepository;

    @Override
    public Iterable<Lekovi> getAll() {
        return medicineRepository.findAll();
    }

    @Override
    public Lekovi getById(Integer id) {
        return medicineRepository.findOne(id);
    }

    @Override
    public void add(Lekovi medicine) {
        medicineRepository.save(medicine);
    }

    @Override
    public void delete(Integer id) {
        medicineRepository.delete(id);
    }

    @Override
    public Iterable<Lekovi> paramSearch(ParamsDto paramsDto) {
        Set<String> keys = paramsDto.getParameterMap().keySet();
        Collection<Object> values = paramsDto.getParameterMap().values();
        String value = (String) values.toArray()[0];
        String key = (String) keys.toArray()[0];
        if(StringUtils.containsIgnoreCase(key, "id")) {
            List<Lekovi> iterable = new ArrayList<>();
            iterable.add(medicineRepository.findOne(Integer.parseInt(value)));
            return iterable;
        }
        Class ca = MedicineRepository.class;
        Method[] methods = ca.getDeclaredMethods();
        Object[] params = new Object[1];
        params[0] = value;
        for(Method method: methods) {
            if(StringUtils.containsIgnoreCase(method.getName(), key)){
                method.setAccessible(true);
                try {
                    return (Iterable<Lekovi>) method.invoke(medicineRepository, params);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}

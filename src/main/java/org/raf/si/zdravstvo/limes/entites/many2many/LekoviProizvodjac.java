package org.raf.si.zdravstvo.limes.entites.many2many;

import org.raf.si.zdravstvo.limes.entites.keys.LekoviProizvodjacPK;

import javax.persistence.*;

@Entity
@Table(name = "lproizvodjac", schema = "limes", catalog = "")
@IdClass(LekoviProizvodjacPK.class)
public class LekoviProizvodjac {
    private int kompanijaId;
    private int lekId;

    @Id
    @Column(name = "Kompanija_Id", nullable = false)
    public int getKompanijaId() {
        return kompanijaId;
    }

    public void setKompanijaId(int kompanijaId) {
        this.kompanijaId = kompanijaId;
    }

    @Id
    @Column(name = "Lek_Id", nullable = false)
    public int getLekId() {
        return lekId;
    }

    public void setLekId(int lekId) {
        this.lekId = lekId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LekoviProizvodjac that = (LekoviProizvodjac) o;

        if (kompanijaId != that.kompanijaId) return false;
        if (lekId != that.lekId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = kompanijaId;
        result = 31 * result + lekId;
        return result;
    }
}

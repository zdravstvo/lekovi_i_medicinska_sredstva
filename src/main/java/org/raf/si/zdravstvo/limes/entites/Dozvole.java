package org.raf.si.zdravstvo.limes.entites;

import org.raf.si.zdravstvo.limes.entites.keys.DozvolePK;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "dozvole", schema = "limes", catalog = "")
@IdClass(DozvolePK.class)
public class Dozvole {
    private int lekId;
    private int komKompanijaId;
    private int kompanijaId;
    private String broj;
    private Date datum;
    private int kolicina;

    @Id
    @Column(name = "Lek_Id", nullable = false)
    public int getLekId() {
        return lekId;
    }

    public void setLekId(int lekId) {
        this.lekId = lekId;
    }

    @Id
    @Column(name = "Kom_Kompanija_Id", nullable = false)
    public int getKomKompanijaId() {
        return komKompanijaId;
    }

    public void setKomKompanijaId(int komKompanijaId) {
        this.komKompanijaId = komKompanijaId;
    }

    @Id
    @Column(name = "Kompanija_Id", nullable = false)
    public int getKompanijaId() {
        return kompanijaId;
    }

    public void setKompanijaId(int kompanijaId) {
        this.kompanijaId = kompanijaId;
    }

    @Basic
    @Column(name = "Broj", nullable = false, length = 30)
    public String getBroj() {
        return broj;
    }

    public void setBroj(String broj) {
        this.broj = broj;
    }

    @Basic
    @Column(name = "Datum", nullable = true)
    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    @Basic
    @Column(name = "Kolicina", nullable = false)
    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dozvole that = (Dozvole) o;

        if (lekId != that.lekId) return false;
        if (komKompanijaId != that.komKompanijaId) return false;
        if (kompanijaId != that.kompanijaId) return false;
        if (kolicina != that.kolicina) return false;
        if (broj != null ? !broj.equals(that.broj) : that.broj != null) return false;
        if (datum != null ? !datum.equals(that.datum) : that.datum != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = lekId;
        result = 31 * result + komKompanijaId;
        result = 31 * result + kompanijaId;
        result = 31 * result + (broj != null ? broj.hashCode() : 0);
        result = 31 * result + (datum != null ? datum.hashCode() : 0);
        result = 31 * result + kolicina;
        return result;
    }
}

package org.raf.si.zdravstvo.limes.dao;

import org.raf.si.zdravstvo.limes.dao.repositories.Crud;
import org.raf.si.zdravstvo.limes.dao.repositories.SponsorRepository;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.dto.SponsorDto;
import org.raf.si.zdravstvo.limes.entites.keys.SponzorPK;
import org.raf.si.zdravstvo.limes.entites.many2many.Sponzor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SponsorDao implements Crud<Sponzor, SponzorPK> {

    @Autowired
    SponsorRepository sponsorRepository;

    @Autowired
    CompanyDao companyDao;

    @Autowired
    MedicalTestingDao medicalTestingDao;

    @Override
    public Iterable<Sponzor> getAll() {
        return sponsorRepository.findAll();
    }

    public List<SponsorDto> smartGetAll() {
        List<SponsorDto> smartSponsors = new ArrayList<>();
        Iterable<Sponzor> sponsors = getAll();
        String companyName, testName;
        for(Sponzor s : sponsors) {
            companyName = companyDao.getById(s.getKompanijaId()).getKompanijaNaziv();
            testName = medicalTestingDao.getById(s.getiResenje()).getiNaziv();
            smartSponsors.add(new SponsorDto(companyName, testName));
        }
        return smartSponsors;
    }

    @Override
    public Sponzor getById(SponzorPK id) {
        return sponsorRepository.findOne(id);
    }

    public SponsorDto smartGetById(SponzorPK id) {
        Sponzor k = getById(id);
        String companyName = companyDao.getById(k.getKompanijaId()).getKompanijaNaziv();
        String testName = medicalTestingDao.getById(k.getiResenje()).getiNaziv();
        return new SponsorDto(companyName, testName);
    }

    @Override
    public void add(Sponzor sponzor) {
        sponsorRepository.save(sponzor);
    }

    @Override
    public void delete(SponzorPK id) {
        sponsorRepository.delete(id);
    }

    @Override
    public Iterable<Sponzor> paramSearch(ParamsDto paramsDto) {
        return null;
    }
}

package org.raf.si.zdravstvo.limes.dao.repositories;

import org.raf.si.zdravstvo.limes.entites.keys.SponzorPK;
import org.raf.si.zdravstvo.limes.entites.many2many.Sponzor;
import org.springframework.data.repository.CrudRepository;

public interface SponsorRepository extends CrudRepository<Sponzor, SponzorPK> {

}

package org.raf.si.zdravstvo.limes.rest.routes;

import org.apache.camel.builder.RouteBuilder;
import org.raf.si.zdravstvo.limes.dao.MIManufacturerDao;
import org.raf.si.zdravstvo.limes.dto.MiManufacturerDto;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.keys.MedicinskaSredstvaProizvodjacPK;
import org.raf.si.zdravstvo.limes.entites.many2many.Klijent;
import org.raf.si.zdravstvo.limes.entites.many2many.MedicinskaSredstvaProizvodjac;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MIManfuacturerRoutes extends RouteBuilder {

    @Autowired
    MIManufacturerDao miManufacturerDao;

    @Override
    public void configure() throws Exception {

        rest("/medical-instrument-manufacturers").description("Medical instrument manufacturer Rest service")

                .get("/getAll").produces("application/json").description("The list of all medical instrument manufacturers")
                    .route().id("medicalInstrumentManufacturerRoute")
                    .bean(miManufacturerDao, "smartGetAll")
                    .endRest()

                .post("/id").type(MedicinskaSredstvaProizvodjacPK.class).outType(MiManufacturerDto.class).description("Get the medical instrument manufacturer by a unique identifier")
                    .route().id("medicalInstrumentManufacturerIdRoute")
                    .bean(miManufacturerDao, "smartGetById(${body})")
                    .endRest()

                .delete("/id").type(MedicinskaSredstvaProizvodjac.class).description("Delete the medical instrument manufacturer by a unique identifier")
                    .route().id("medicalInstrumentManufacturerDeleteRoute")
                    .bean(miManufacturerDao, "delete(${body})")
                    .endRest()

                .post("/params").type(ParamsDto.class).produces("application/json").description("Get all medical instrument manufacturers that fulfill the criteria")
                    .route().id("medicalInstrumentManufacturerParamsRoute")
                    .bean(miManufacturerDao, "paramSearch(${body})")
                    .endRest()

                .post("/insert").type(Klijent.class).description("Add new medical instrument manufacturer to the database")
                    .route().id("medicalInstrumentManufacturerAddRoute")
                    .bean(miManufacturerDao, "add(${body})")
                    .endRest();

    }
}

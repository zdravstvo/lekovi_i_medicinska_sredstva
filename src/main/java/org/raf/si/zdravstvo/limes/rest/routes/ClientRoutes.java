package org.raf.si.zdravstvo.limes.rest.routes;

import org.apache.camel.builder.RouteBuilder;
import org.raf.si.zdravstvo.limes.dao.ClientDao;
import org.raf.si.zdravstvo.limes.dto.ClientDto;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.keys.KlijentPK;
import org.raf.si.zdravstvo.limes.entites.many2many.Klijent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClientRoutes extends RouteBuilder {

    @Autowired
    ClientDao clientDao;

    @Override
    public void configure() throws Exception {

        rest("/clients").description("Client Rest service")

                .get("/getAll").produces("application/json").description("The list of all clients")
                    .route().id("clientRoute")
                    .bean(clientDao, "smartGetAll")
                    .endRest()

                .post("/id").type(KlijentPK.class).outType(ClientDto.class).description("Get the client by a unique identifier")
                    .route().id("clientIdRoute")
                    .bean(clientDao, "smartGetById(${body})")
                    .endRest()

                .delete("/id").type(Klijent.class).description("Delete the client by a unique identifier")
                    .route().id("clientDeleteRoute")
                    .bean(clientDao, "delete(${body})")
                    .endRest()

                .post("/params").type(ParamsDto.class).produces("application/json").description("Get all clients that fulfill the criteria")
                    .route().id("clientParamsRoute")
                    .bean(clientDao, "paramSearch(${body})")
                    .endRest()

                .post("/insert").type(Klijent.class).description("Add new client to the database")
                    .route().id("clientAddRoute")
                    .bean(clientDao, "add(${body})")
                    .endRest();

    }
}

package org.raf.si.zdravstvo.limes.entites.keys;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class NosilacPK implements Serializable {
    private int kompanijaId;
    private String pmResenje;

    @Column(name = "Kompanija_Id", nullable = false)
    @Id
    public int getKompanijaId() {
        return kompanijaId;
    }

    public void setKompanijaId(int kompanijaId) {
        this.kompanijaId = kompanijaId;
    }

    @Column(name = "PM_resenje", nullable = false, length = 50)
    @Id
    public String getPmResenje() {
        return pmResenje;
    }

    public void setPmResenje(String pmResenje) {
        this.pmResenje = pmResenje;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NosilacPK that = (NosilacPK) o;

        if (kompanijaId != that.kompanijaId) return false;
        if (pmResenje != null ? !pmResenje.equals(that.pmResenje) : that.pmResenje != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = kompanijaId;
        result = 31 * result + (pmResenje != null ? pmResenje.hashCode() : 0);
        return result;
    }
}

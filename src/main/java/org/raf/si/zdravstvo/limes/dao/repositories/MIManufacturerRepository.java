package org.raf.si.zdravstvo.limes.dao.repositories;

import org.raf.si.zdravstvo.limes.entites.keys.MedicinskaSredstvaProizvodjacPK;
import org.raf.si.zdravstvo.limes.entites.many2many.MedicinskaSredstvaProizvodjac;
import org.springframework.data.repository.CrudRepository;

public interface MIManufacturerRepository extends CrudRepository<MedicinskaSredstvaProizvodjac, MedicinskaSredstvaProizvodjacPK> {

}

package org.raf.si.zdravstvo.limes.dto;

public class CarrierDto {

    private String companyName;
    private String pmSolution;

    public CarrierDto() {
    }

    public CarrierDto(String companyName, String pmSolution) {
        this.companyName = companyName;
        this.pmSolution = pmSolution;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPmSolution() {
        return pmSolution;
    }

    public void setPmSolution(String pmSolution) {
        this.pmSolution = pmSolution;
    }
}

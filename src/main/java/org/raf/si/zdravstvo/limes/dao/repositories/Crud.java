package org.raf.si.zdravstvo.limes.dao.repositories;

import org.raf.si.zdravstvo.limes.dto.ParamsDto;

public interface Crud<T, I> {

    Iterable<T> getAll();

    T getById(I id);

    void add(T entity);

    void delete(I id);

    Iterable<T> paramSearch(ParamsDto paramsDto);
}

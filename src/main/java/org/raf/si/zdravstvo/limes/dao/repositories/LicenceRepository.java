package org.raf.si.zdravstvo.limes.dao.repositories;

import org.raf.si.zdravstvo.limes.entites.Dozvole;
import org.raf.si.zdravstvo.limes.entites.keys.DozvolePK;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface LicenceRepository extends CrudRepository<Dozvole, DozvolePK> {

    public List<Dozvole> findByDatumBetween(Date fromDate, Date toDate);

}

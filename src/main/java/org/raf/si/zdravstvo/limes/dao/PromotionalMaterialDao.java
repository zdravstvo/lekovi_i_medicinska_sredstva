package org.raf.si.zdravstvo.limes.dao;

import org.apache.commons.lang3.StringUtils;
import org.raf.si.zdravstvo.limes.dao.repositories.CompanyRepository;
import org.raf.si.zdravstvo.limes.dao.repositories.Crud;
import org.raf.si.zdravstvo.limes.dao.repositories.PromotionalMaterialRepository;
import org.raf.si.zdravstvo.limes.dto.PMDto;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.dto.PeriodDto;
import org.raf.si.zdravstvo.limes.entites.Kompanija;
import org.raf.si.zdravstvo.limes.entites.PromotivniMaterijal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class PromotionalMaterialDao implements Crud<PromotivniMaterijal, String> {

    @Autowired
    PromotionalMaterialRepository promotionalMaterialRepository;

    @Autowired
    MedicineDao medicineDao;

    @Override
    public Iterable<PromotivniMaterijal> getAll() {
        return promotionalMaterialRepository.findAll();
    }

    @Override
    public PromotivniMaterijal getById(String id) {
        return promotionalMaterialRepository.findOne(id);
    }

    @Override
    public void add(PromotivniMaterijal medicalInstrument) {
        promotionalMaterialRepository.save(medicalInstrument);
    }

    @Override
    public void delete(String id) {
        promotionalMaterialRepository.delete(id);
    }

    @Override
    public Iterable<PromotivniMaterijal> paramSearch(ParamsDto paramsDto) {
        Set<String> keys = paramsDto.getParameterMap().keySet();
        Collection<Object> values = paramsDto.getParameterMap().values();
        String value = (String) values.toArray()[0];
        String key = (String) keys.toArray()[0];
        if (StringUtils.containsIgnoreCase(key, "id")) {
            List<PromotivniMaterijal> iterable = new ArrayList<>();
            iterable.add(promotionalMaterialRepository.findOne(value));
            return iterable;
        }
        Class ca = PromotionalMaterialRepository.class;
        Method[] methods = ca.getDeclaredMethods();
        Object[] params = new Object[1];
        if (StringUtils.containsIgnoreCase(key, "vazi")) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date date = sdf.parse(value);
                params[0] = date;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            params[0] = value;
        }
        for (Method method : methods) {
            if (StringUtils.containsIgnoreCase(method.getName(), key)) {
                method.setAccessible(true);
                try {
                    return (Iterable<PromotivniMaterijal>) method.invoke(promotionalMaterialRepository, params);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public List<PMDto> getReport(PeriodDto periodDto) {
        HashMap<String, Integer> report = new HashMap<>();
        Iterable<PromotivniMaterijal> allPM = getAll();
        List<PMDto> pmDtoList = new ArrayList<>();
        String medicineName;

        for (PromotivniMaterijal pm: allPM) {
            if (pm.getVaziOd().before(periodDto.getToDate()) && pm.getVaziDo().after(periodDto.getFromDate())) {
                medicineName = medicineDao.getById(pm.getLekId()).getLekNaziv();
                if (report.containsKey(medicineName)) {
                    int amount = report.get(medicineName);
                    amount++;
                    report.put(medicineName, amount);
                }
                else {
                    report.put(medicineName, 1);
                }
            }
        }

        for(String key: report.keySet()) {
            pmDtoList.add(new PMDto(key, report.get(key)));
        }

        return pmDtoList;
    }

    public HashMap<String, Integer> getPurposeReport(PeriodDto periodDto) {
        HashMap<String, Integer> report = new HashMap<>();
        Iterable<PromotivniMaterijal> allPM = getAll();

        for (PromotivniMaterijal pm: allPM) {
            if (pm.getVaziOd().before(periodDto.getToDate()) && pm.getVaziDo().after(periodDto.getFromDate())) {
                String purpose = pm.getNamenaPromocije();
                if (report.containsKey(purpose)) {
                    int amount = report.get(purpose);
                    amount += 1;
                    report.put(purpose, amount);
                }
                else {
                    report.put(purpose, 1);
                }
            }
        }

        return report;
    }
}

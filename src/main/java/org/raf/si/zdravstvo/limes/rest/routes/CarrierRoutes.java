package org.raf.si.zdravstvo.limes.rest.routes;

import org.apache.camel.builder.RouteBuilder;
import org.raf.si.zdravstvo.limes.dao.CarrierDao;
import org.raf.si.zdravstvo.limes.dto.CarrierDto;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.keys.NosilacPK;
import org.raf.si.zdravstvo.limes.entites.many2many.Nosilac;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CarrierRoutes extends RouteBuilder {

    @Autowired
    CarrierDao carrierDao;

    @Override
    public void configure() throws Exception {

        rest("/carrier").description("Carrier Rest service")

                .get("/getAll").produces("application/json").description("The list of all carriers")
                    .route().id("carrierRoute")
                    .bean(carrierDao, "smartGetAll")
                    .endRest()

                .post("/id").type(NosilacPK.class).outType(CarrierDto.class).description("Get the carrier by a unique identifier")
                    .route().id("carrierIdRoute")
                    .bean(carrierDao, "smartGetById(${body})")
                    .endRest()

                .delete("/id").type(Nosilac.class).description("Delete the carrier by a unique identifier")
                    .route().id("carrierDeleteRoute")
                    .bean(carrierDao, "delete(${body})")
                    .endRest()

                .post("/params").type(ParamsDto.class).produces("application/json").description("Get all carrier that fulfill the criteria")
                    .route().id("carrierParamsRoute")
                    .bean(carrierDao, "paramSearch(${body})")
                    .endRest()

                .post("/insert").type(Nosilac.class).description("Add new carrier to the database")
                    .route().id("carrierAddRoute")
                    .bean(carrierDao, "add(${body})")
                    .endRest();

    }

}

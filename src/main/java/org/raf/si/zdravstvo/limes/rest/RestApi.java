package org.raf.si.zdravstvo.limes.rest;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

@Component
public class RestApi extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        restConfiguration()
                .contextPath("/limes-api").apiContextPath("/api-doc")
                .apiProperty("api.title", "LiMeS REST API")
                .apiProperty("api.version", "1.0.0")
                .apiContextRouteId("doc-api")
                .enableCORS(true)
                .bindingMode(RestBindingMode.json);

    }
}

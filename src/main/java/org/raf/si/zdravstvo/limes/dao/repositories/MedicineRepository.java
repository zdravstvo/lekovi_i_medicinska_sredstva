package org.raf.si.zdravstvo.limes.dao.repositories;

import org.raf.si.zdravstvo.limes.entites.Lekovi;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface MedicineRepository extends CrudRepository<Lekovi, Integer> {

    public Iterable<Lekovi> findByLekNaziv(String param);

    public Iterable<Lekovi> findByAtcSifra(String param);

    public Iterable<Lekovi> findByOblik(String param);

    public Iterable<Lekovi> findByLekVrsta(String param);

    public Iterable<Lekovi> findByPakovanje(String param);

    public Iterable<Lekovi> findByRezimIzdavanja(String param);


}

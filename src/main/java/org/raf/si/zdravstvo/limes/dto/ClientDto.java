package org.raf.si.zdravstvo.limes.dto;

public class ClientDto {

    private String companyName;
    private String testName;

    public ClientDto() {
    }

    public ClientDto(String companyName, String testName) {
        this.companyName = companyName;
        this.testName = testName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }
}

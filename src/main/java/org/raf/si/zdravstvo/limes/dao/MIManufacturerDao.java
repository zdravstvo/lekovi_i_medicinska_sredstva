package org.raf.si.zdravstvo.limes.dao;

import org.raf.si.zdravstvo.limes.dao.repositories.Crud;
import org.raf.si.zdravstvo.limes.dao.repositories.MIManufacturerRepository;
import org.raf.si.zdravstvo.limes.dto.MiManufacturerDto;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.keys.MedicinskaSredstvaProizvodjacPK;
import org.raf.si.zdravstvo.limes.entites.many2many.MedicinskaSredstvaProizvodjac;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MIManufacturerDao implements Crud<MedicinskaSredstvaProizvodjac, MedicinskaSredstvaProizvodjacPK> {

    @Autowired
    MIManufacturerRepository miManufacturerRepository;

    @Autowired
    CompanyDao companyDao;

    @Autowired
    MedicalInstrumentsDao medicalInstrumentsDao;

    @Override
    public Iterable<MedicinskaSredstvaProizvodjac> getAll() {
        return miManufacturerRepository.findAll();
    }

    public List<MiManufacturerDto> smartGetAll() {
        List<MiManufacturerDto> smartManufacturers = new ArrayList<>();
        Iterable<MedicinskaSredstvaProizvodjac> clients = getAll();
        String companyName, manufacturerName;
        for(MedicinskaSredstvaProizvodjac k : clients) {
            companyName = companyDao.getById(k.getKompanijaId()).getKompanijaNaziv();
            manufacturerName = medicalInstrumentsDao.getById(k.getMsId()).getMsNaziv();
            smartManufacturers.add(new MiManufacturerDto(companyName, manufacturerName));
        }
        return smartManufacturers;
    }

    @Override
    public MedicinskaSredstvaProizvodjac getById(MedicinskaSredstvaProizvodjacPK id) {
        return miManufacturerRepository.findOne(id);
    }

    public MiManufacturerDto smartGetById(MedicinskaSredstvaProizvodjacPK id) {
        MedicinskaSredstvaProizvodjac k = getById(id);
        String companyName = companyDao.getById(k.getKompanijaId()).getKompanijaNaziv();
        String manufacturerName = medicalInstrumentsDao.getById(k.getMsId()).getMsNaziv();
        return new MiManufacturerDto(companyName, manufacturerName);
    }

    @Override
    public void add(MedicinskaSredstvaProizvodjac proizvodjac) {
        miManufacturerRepository.save(proizvodjac);
    }

    @Override
    public void delete(MedicinskaSredstvaProizvodjacPK id) {
        miManufacturerRepository.delete(id);
    }

    @Override
    public Iterable<MedicinskaSredstvaProizvodjac> paramSearch(ParamsDto paramsDto) {
        return null;
    }
}

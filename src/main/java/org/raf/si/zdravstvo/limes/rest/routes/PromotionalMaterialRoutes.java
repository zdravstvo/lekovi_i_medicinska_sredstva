package org.raf.si.zdravstvo.limes.rest.routes;

import org.apache.camel.builder.RouteBuilder;
import org.raf.si.zdravstvo.limes.dao.PromotionalMaterialDao;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.dto.PeriodDto;
import org.raf.si.zdravstvo.limes.entites.PromotivniMaterijal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PromotionalMaterialRoutes extends RouteBuilder {

    @Autowired
    PromotionalMaterialDao promotionalMaterialDao;

    @Override
    public void configure() throws Exception {

        rest("/promotional-materials").description("Promotional materials Rest service")

                .get("/getAll").produces("application/json").description("The list of all promotional materials")
                .route().id("promotionalMaterialsRoute")
                .bean(promotionalMaterialDao, "getAll")
                .endRest()

                .delete("/{id}").description("Delete the promotional material by a unique identifier")
                .route().id("promotionalMaterialsDeleteRoute")
                .bean(promotionalMaterialDao, "delete(${headers.id})")
                .endRest()

                .post("/params").type(ParamsDto.class).produces("application/json").description("Get all PMs that fulfill the criteria")
                .route().id("promotionalMaterialsParamsRoute")
                .bean(promotionalMaterialDao, "paramSearch(${body})")
                .endRest()

                .post("/insert").type(PromotivniMaterijal.class).description("Add new promotional material to the database")
                .route().id("promotionalMaterialsAddRoute")
                .bean(promotionalMaterialDao, "add(${body})")
                .endRest()

                .post("/report").type(PeriodDto.class).produces("application/json").description("Report for promotional material")
                .route().id("promotionalMaterialsReport")
                .bean(promotionalMaterialDao, "getReport(${body})")
                .endRest()

                .post("/purpose-report").type(PeriodDto.class).produces("application/json").description("Purpose Report for promotional material")
                .route().id("promotionalMaterialsPurposeReport")
                .bean(promotionalMaterialDao, "getPurposeReport(${body})")
                .endRest();

    }
}

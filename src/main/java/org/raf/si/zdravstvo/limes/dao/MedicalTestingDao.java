package org.raf.si.zdravstvo.limes.dao;

import org.apache.commons.lang3.StringUtils;
import org.raf.si.zdravstvo.limes.dao.repositories.Crud;
import org.raf.si.zdravstvo.limes.dao.repositories.MedicalInstrumentRepository;
import org.raf.si.zdravstvo.limes.dao.repositories.MedicalTestingRepository;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.Ispitivanje;
import org.raf.si.zdravstvo.limes.entites.MedicinskaSredstva;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class MedicalTestingDao implements Crud<Ispitivanje, String> {

    @Autowired
    MedicalTestingRepository medicalTestingRepository;

    @Override
    public Iterable<Ispitivanje> getAll() {
        return medicalTestingRepository.findAll();
    }

    @Override
    public Ispitivanje getById(String id) {
        return medicalTestingRepository.findOne(id);
    }

    @Override
    public void add(Ispitivanje company) {
        medicalTestingRepository.save(company);
    }

    @Override
    public void delete(String id) {
        medicalTestingRepository.delete(id);
    }

    @Override
    public Iterable<Ispitivanje> paramSearch(ParamsDto paramsDto) {
        Set<String> keys = paramsDto.getParameterMap().keySet();
        Collection<Object> values = paramsDto.getParameterMap().values();
        String value = (String) values.toArray()[0];
        String key = (String) keys.toArray()[0];
        Class ca = MedicalTestingRepository.class;
        Method[] methods = ca.getDeclaredMethods();
        Object[] params = new Object[1];
        if (StringUtils.containsIgnoreCase(key, "izdato")) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date date = sdf.parse(value);
                params[0] = date;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            params[0] = value;
        }
        for (Method method : methods) {
            if (StringUtils.containsIgnoreCase(method.getName(), key)) {
                method.setAccessible(true);
                try {
                    return (Iterable<Ispitivanje>) method.invoke(medicalTestingRepository, params);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}

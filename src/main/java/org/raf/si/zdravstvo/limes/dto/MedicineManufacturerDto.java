package org.raf.si.zdravstvo.limes.dto;

public class MedicineManufacturerDto {

    private String companyName;
    private String medicineName;

    public MedicineManufacturerDto() {
    }

    public MedicineManufacturerDto(String companyName, String medicineName) {
        this.companyName = companyName;
        this.medicineName = medicineName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

}

package org.raf.si.zdravstvo.limes.rest.routes;

import org.apache.camel.builder.RouteBuilder;
import org.raf.si.zdravstvo.limes.dao.LicenceDao;
import org.raf.si.zdravstvo.limes.dto.LicenceDto;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.dto.PeriodDto;
import org.raf.si.zdravstvo.limes.entites.Dozvole;
import org.raf.si.zdravstvo.limes.entites.keys.DozvolePK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LicenceRoutes extends RouteBuilder {

    @Autowired
    LicenceDao licenceDao;

    @Override
    public void configure() throws Exception {

        rest("/licences").description("Licence Rest service")

                .get("/getAll").produces("application/json").description("The list of all licences")
                    .route().id("licencesRoute")
                    .bean(licenceDao, "smartGetAll")
                    .endRest()

                .post("/id").type(DozvolePK.class).outType(LicenceDto.class).description("Get the licence by a unique identifier")
                .route().id("licenceIdRoute")
                .bean(licenceDao, "smartGetById(${body})")
                .endRest()

                .delete("/id").type(Dozvole.class).description("Delete the licence by a unique identifier")
                .route().id("licenceDeleteRoute")
                .bean(licenceDao, "delete(${body})")
                .endRest()

                .post("/params").type(ParamsDto.class).produces("application/json").description("Get all licences that fulfill the criteria")
                .route().id("licenceParamsRoute")
                .bean(licenceDao, "paramSearch(${body})")
                .endRest()

                .post("/insert").type(Dozvole.class).description("Add new licence to the database")
                .route().id("licenceAddRoute")
                .bean(licenceDao, "add(${body})")
                .endRest()

                .post("/report").type(PeriodDto.class).produces("application/json").description("Report for licences")
                .route().id("licencesReport")
                .bean(licenceDao, "getReport(${body})")
                .endRest();
    }
}

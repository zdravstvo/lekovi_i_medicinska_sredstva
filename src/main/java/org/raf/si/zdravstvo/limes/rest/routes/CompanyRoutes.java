package org.raf.si.zdravstvo.limes.rest.routes;

import org.apache.camel.builder.RouteBuilder;
import org.raf.si.zdravstvo.limes.dao.CompanyDao;
import org.raf.si.zdravstvo.limes.dao.MedicineDao;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.Kompanija;
import org.raf.si.zdravstvo.limes.entites.Lekovi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CompanyRoutes extends RouteBuilder {

    @Autowired
    CompanyDao companyDao;

    @Override
    public void configure() throws Exception {

        rest("/companies").description("Company Rest service")

                .get("/getAll").produces("application/json").description("The list of all companies")
                    .route().id("companiesRoute")
                    .bean(companyDao, "getAll")
                    .endRest()

                .delete("/{id}").description("Delete the company by a unique identifier")
                    .route().id("companyDeleteRoute")
                    .bean(companyDao, "delete(${headers.id})")
                    .endRest()

                .post("/params").type(ParamsDto.class).produces("application/json").description("Get all companies that fulfill the criteria")
                    .route().id("companyParamsRoute")
                    .bean(companyDao, "paramSearch(${body})")
                    .endRest()

                .post("/insert").type(Kompanija.class).description("Add new company to the database")
                    .route().id("companyAddRoute")
                    .bean(companyDao, "add(${body})")
                    .endRest();
    }
}

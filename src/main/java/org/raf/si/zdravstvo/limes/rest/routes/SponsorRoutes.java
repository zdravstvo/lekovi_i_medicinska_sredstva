package org.raf.si.zdravstvo.limes.rest.routes;

import org.apache.camel.builder.RouteBuilder;
import org.raf.si.zdravstvo.limes.dao.SponsorDao;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.dto.SponsorDto;
import org.raf.si.zdravstvo.limes.entites.keys.SponzorPK;
import org.raf.si.zdravstvo.limes.entites.many2many.Sponzor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SponsorRoutes extends RouteBuilder {

    @Autowired
    SponsorDao sponsorDao;

    @Override
    public void configure() throws Exception {

        rest("/sponsors").description("Sponsor Rest service")

                .get("/getAll").produces("application/json").description("The list of all clients")
                    .route().id("sponsorRoute")
                    .bean(sponsorDao, "smartGetAll")
                    .endRest()

                .post("/id").type(SponzorPK.class).outType(SponsorDto.class).description("Get the sponsor by a unique identifier")
                    .route().id("sponsorIdRoute")
                    .bean(sponsorDao, "smartGetById(${body})")
                    .endRest()

                .delete("/id").type(Sponzor.class).description("Delete the sponsor by a unique identifier")
                    .route().id("sponsorDeleteRoute")
                    .bean(sponsorDao, "delete(${body})")
                    .endRest()

                .post("/params").type(ParamsDto.class).produces("application/json").description("Get all sponsors that fulfill the criteria")
                    .route().id("sponsorParamsRoute")
                    .bean(sponsorDao, "paramSearch(${body})")
                    .endRest()

                .post("/insert").type(Sponzor.class).description("Add new sponsor to the database")
                    .route().id("sponsorAddRoute")
                    .bean(sponsorDao, "add(${body})")
                    .endRest();

    }
}

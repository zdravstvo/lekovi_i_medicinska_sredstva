package org.raf.si.zdravstvo.limes.entites.keys;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class MedicinskaSredstvaProizvodjacPK implements Serializable {
    private int kompanijaId;
    private int msId;

    @Column(name = "Kompanija_Id", nullable = false)
    @Id
    public int getKompanijaId() {
        return kompanijaId;
    }

    public void setKompanijaId(int kompanijaId) {
        this.kompanijaId = kompanijaId;
    }

    @Column(name = "MS_Id", nullable = false)
    @Id
    public int getMsId() {
        return msId;
    }

    public void setMsId(int msId) {
        this.msId = msId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MedicinskaSredstvaProizvodjacPK that = (MedicinskaSredstvaProizvodjacPK) o;

        if (kompanijaId != that.kompanijaId) return false;
        if (msId != that.msId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = kompanijaId;
        result = 31 * result + msId;
        return result;
    }
}

package org.raf.si.zdravstvo.limes.entites;

import javax.persistence.*;

@Entity
@Table(name = "kompanija", schema = "limes", catalog = "")
public class Kompanija {
    private int kompanijaId;
    private String kompanijaNaziv;
    private String tip;
    private String ulica;
    private String grad;
    private String drzava;

    @Id
    @Column(name = "Kompanija_Id", nullable = false)
    public int getKompanijaId() {
        return kompanijaId;
    }

    public void setKompanijaId(int kompanijaId) {
        this.kompanijaId = kompanijaId;
    }

    @Basic
    @Column(name = "Kompanija_naziv", nullable = false, length = 40)
    public String getKompanijaNaziv() {
        return kompanijaNaziv;
    }

    public void setKompanijaNaziv(String kompanijaNaziv) {
        this.kompanijaNaziv = kompanijaNaziv;
    }

    @Basic
    @Column(name = "Tip", nullable = true, length = 1)
    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    @Basic
    @Column(name = "Ulica", nullable = true, length = 20)
    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    @Basic
    @Column(name = "Grad", nullable = true, length = 20)
    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    @Basic
    @Column(name = "Drzava", nullable = true, length = 20)
    public String getDrzava() {
        return drzava;
    }

    public void setDrzava(String drzava) {
        this.drzava = drzava;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Kompanija that = (Kompanija) o;

        if (kompanijaId != that.kompanijaId) return false;
        if (kompanijaNaziv != null ? !kompanijaNaziv.equals(that.kompanijaNaziv) : that.kompanijaNaziv != null)
            return false;
        if (tip != null ? !tip.equals(that.tip) : that.tip != null) return false;
        if (ulica != null ? !ulica.equals(that.ulica) : that.ulica != null) return false;
        if (grad != null ? !grad.equals(that.grad) : that.grad != null) return false;
        if (drzava != null ? !drzava.equals(that.drzava) : that.drzava != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = kompanijaId;
        result = 31 * result + (kompanijaNaziv != null ? kompanijaNaziv.hashCode() : 0);
        result = 31 * result + (tip != null ? tip.hashCode() : 0);
        result = 31 * result + (ulica != null ? ulica.hashCode() : 0);
        result = 31 * result + (grad != null ? grad.hashCode() : 0);
        result = 31 * result + (drzava != null ? drzava.hashCode() : 0);
        return result;
    }
}

package org.raf.si.zdravstvo.limes.entites;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "ispitivanje", schema = "limes", catalog = "")
public class Ispitivanje {
    private String iResenje;
    private String iNaziv;
    private String opis;
    private String brojProtokola;
    private Date iIzdato;
    private String status;

    @Id
    @Column(name = "I_resenje", nullable = false, length = 50)
    public String getiResenje() {
        return iResenje;
    }

    public void setiResenje(String iResenje) {
        this.iResenje = iResenje;
    }

    @Basic
    @Column(name = "I_naziv", nullable = false, length = 40)
    public String getiNaziv() {
        return iNaziv;
    }

    public void setiNaziv(String iNaziv) {
        this.iNaziv = iNaziv;
    }

    @Basic
    @Column(name = "Opis", nullable = true, length = 50)
    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @Basic
    @Column(name = "Broj_protokola", nullable = true, length = 40)
    public String getBrojProtokola() {
        return brojProtokola;
    }

    public void setBrojProtokola(String brojProtokola) {
        this.brojProtokola = brojProtokola;
    }

    @Basic
    @Column(name = "I_izdato", nullable = true)
    public Date getiIzdato() {
        return iIzdato;
    }

    public void setiIzdato(Date iIzdato) {
        this.iIzdato = iIzdato;
    }

    @Basic
    @Column(name = "Status", nullable = true, length = 1)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ispitivanje that = (Ispitivanje) o;

        if (iResenje != null ? !iResenje.equals(that.iResenje) : that.iResenje != null) return false;
        if (iNaziv != null ? !iNaziv.equals(that.iNaziv) : that.iNaziv != null) return false;
        if (opis != null ? !opis.equals(that.opis) : that.opis != null) return false;
        if (brojProtokola != null ? !brojProtokola.equals(that.brojProtokola) : that.brojProtokola != null)
            return false;
        if (iIzdato != null ? !iIzdato.equals(that.iIzdato) : that.iIzdato != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = iResenje != null ? iResenje.hashCode() : 0;
        result = 31 * result + (iNaziv != null ? iNaziv.hashCode() : 0);
        result = 31 * result + (opis != null ? opis.hashCode() : 0);
        result = 31 * result + (brojProtokola != null ? brojProtokola.hashCode() : 0);
        result = 31 * result + (iIzdato != null ? iIzdato.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }
}

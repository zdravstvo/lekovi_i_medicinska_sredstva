package org.raf.si.zdravstvo.limes.entites.keys;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class LekoviProizvodjacPK implements Serializable {
    private int kompanijaId;
    private int lekId;

    @Column(name = "Kompanija_Id", nullable = false)
    @Id
    public int getKompanijaId() {
        return kompanijaId;
    }

    public void setKompanijaId(int kompanijaId) {
        this.kompanijaId = kompanijaId;
    }

    @Column(name = "Lek_Id", nullable = false)
    @Id
    public int getLekId() {
        return lekId;
    }

    public void setLekId(int lekId) {
        this.lekId = lekId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LekoviProizvodjacPK that = (LekoviProizvodjacPK) o;

        if (kompanijaId != that.kompanijaId) return false;
        if (lekId != that.lekId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = kompanijaId;
        result = 31 * result + lekId;
        return result;
    }
}

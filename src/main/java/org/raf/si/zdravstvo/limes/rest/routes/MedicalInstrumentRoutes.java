package org.raf.si.zdravstvo.limes.rest.routes;

import org.apache.camel.builder.RouteBuilder;
import org.raf.si.zdravstvo.limes.dao.MedicalInstrumentsDao;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.MedicinskaSredstva;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MedicalInstrumentRoutes extends RouteBuilder {

    @Autowired
    MedicalInstrumentsDao medicalInstrumentsDao;

    @Override
    public void configure() throws Exception {

        rest("/medical-instruments").description("Medical instrument Rest service")

                .get("/getAll").produces("application/json").description("The list of all medical instruments")
                    .route().id("medicalInstrumentsRoute")
                    .bean(medicalInstrumentsDao, "getAll")
                    .endRest()

                .delete("/{id}").description("Delete the medical instrument by a unique identifier")
                    .route().id("medicalInstrumentsDeleteRoute")
                    .bean(medicalInstrumentsDao, "delete(${headers.id})")
                    .endRest()

                .post("/params").type(ParamsDto.class).produces("application/json").description("Get all MI that fulfill the criteria")
                    .route().id("medicalInstrumentsParamsRoute")
                    .bean(medicalInstrumentsDao, "paramSearch(${body})")
                    .endRest()

                .post("/insert").type(MedicinskaSredstva.class).description("Add new medical instrument to the database")
                    .route().id("medicalInstrumentsAddRoute")
                    .bean(medicalInstrumentsDao, "add(${body})")
                    .endRest();

    }
}

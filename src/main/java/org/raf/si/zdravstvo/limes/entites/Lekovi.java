package org.raf.si.zdravstvo.limes.entites;

import javax.persistence.*;

@Entity
@Table(name = "lekovi", schema = "limes", catalog = "")
public class Lekovi {
    private int lekId;
    private String lekNaziv;
    private String lekVrsta;
    private String atcSifra;
    private double jacina;
    private String oblik;
    private String pakovanje;
    private String rezimIzdavanja;

    @Id
    @Column(name = "Lek_Id", nullable = false)
    public int getLekId() {
        return lekId;
    }

    public void setLekId(int lekId) {
        this.lekId = lekId;
    }

    @Basic
    @Column(name = "Lek_naziv", nullable = false, length = 40)
    public String getLekNaziv() {
        return lekNaziv;
    }

    public void setLekNaziv(String lekNaziv) {
        this.lekNaziv = lekNaziv;
    }

    @Basic
    @Column(name = "Lek_vrsta", nullable = false, length = 40)
    public String getLekVrsta() {
        return lekVrsta;
    }

    public void setLekVrsta(String lekVrsta) {
        this.lekVrsta = lekVrsta;
    }

    @Basic
    @Column(name = "ATC_sifra", nullable = false, length = 30)
    public String getAtcSifra() {
        return atcSifra;
    }

    public void setAtcSifra(String atcSifra) {
        this.atcSifra = atcSifra;
    }

    @Basic
    @Column(name = "Jacina", nullable = false, precision = 0)
    public double getJacina() {
        return jacina;
    }

    public void setJacina(double jacina) {
        this.jacina = jacina;
    }

    @Basic
    @Column(name = "Oblik", nullable = true, length = 20)
    public String getOblik() {
        return oblik;
    }

    public void setOblik(String oblik) {
        this.oblik = oblik;
    }

    @Basic
    @Column(name = "Pakovanje", nullable = true, length = 20)
    public String getPakovanje() {
        return pakovanje;
    }

    public void setPakovanje(String pakovanje) {
        this.pakovanje = pakovanje;
    }

    @Basic
    @Column(name = "Rezim_izdavanja", nullable = true, length = 2)
    public String getRezimIzdavanja() {
        return rezimIzdavanja;
    }

    public void setRezimIzdavanja(String rezimIzdavanja) {
        this.rezimIzdavanja = rezimIzdavanja;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lekovi that = (Lekovi) o;

        if (lekId != that.lekId) return false;
        if (Double.compare(that.jacina, jacina) != 0) return false;
        if (lekNaziv != null ? !lekNaziv.equals(that.lekNaziv) : that.lekNaziv != null) return false;
        if (lekVrsta != null ? !lekVrsta.equals(that.lekVrsta) : that.lekVrsta != null) return false;
        if (atcSifra != null ? !atcSifra.equals(that.atcSifra) : that.atcSifra != null) return false;
        if (oblik != null ? !oblik.equals(that.oblik) : that.oblik != null) return false;
        if (pakovanje != null ? !pakovanje.equals(that.pakovanje) : that.pakovanje != null) return false;
        if (rezimIzdavanja != null ? !rezimIzdavanja.equals(that.rezimIzdavanja) : that.rezimIzdavanja != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = lekId;
        result = 31 * result + (lekNaziv != null ? lekNaziv.hashCode() : 0);
        result = 31 * result + (lekVrsta != null ? lekVrsta.hashCode() : 0);
        result = 31 * result + (atcSifra != null ? atcSifra.hashCode() : 0);
        temp = Double.doubleToLongBits(jacina);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (oblik != null ? oblik.hashCode() : 0);
        result = 31 * result + (pakovanje != null ? pakovanje.hashCode() : 0);
        result = 31 * result + (rezimIzdavanja != null ? rezimIzdavanja.hashCode() : 0);
        return result;
    }
}

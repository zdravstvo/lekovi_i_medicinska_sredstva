package org.raf.si.zdravstvo.limes.entites.many2many;

import org.raf.si.zdravstvo.limes.entites.keys.MedicinskaSredstvaProizvodjacPK;

import javax.persistence.*;

@Entity
@Table(name = "msproizvodjac", schema = "limes", catalog = "")
@IdClass(MedicinskaSredstvaProizvodjacPK.class)
public class MedicinskaSredstvaProizvodjac {
    private int kompanijaId;
    private int msId;

    @Id
    @Column(name = "Kompanija_Id", nullable = false)
    public int getKompanijaId() {
        return kompanijaId;
    }

    public void setKompanijaId(int kompanijaId) {
        this.kompanijaId = kompanijaId;
    }

    @Id
    @Column(name = "MS_Id", nullable = false)
    public int getMsId() {
        return msId;
    }

    public void setMsId(int msId) {
        this.msId = msId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MedicinskaSredstvaProizvodjac that = (MedicinskaSredstvaProizvodjac) o;

        if (kompanijaId != that.kompanijaId) return false;
        if (msId != that.msId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = kompanijaId;
        result = 31 * result + msId;
        return result;
    }
}

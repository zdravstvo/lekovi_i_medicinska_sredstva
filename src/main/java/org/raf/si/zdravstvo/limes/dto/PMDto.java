package org.raf.si.zdravstvo.limes.dto;

public class PMDto {

    private String medicineName;
    private int number;

    public PMDto() {
    }

    public PMDto(String medicineName, int number) {
        this.medicineName = medicineName;
        this.number = number;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}

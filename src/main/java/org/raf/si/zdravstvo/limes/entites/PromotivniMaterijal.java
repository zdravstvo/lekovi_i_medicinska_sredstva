package org.raf.si.zdravstvo.limes.entites;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "promotivni_materijal", schema = "limes", catalog = "")
public class PromotivniMaterijal {
    private int lekId;
    private String pmResenje;
    private String materijal;
    private String namenaPromocije;
    private Date vaziOd;
    private Date vaziDo;

    @Id
    @Column(name = "PM_resenje", nullable = false, length = 50)
    public String getPmResenje() {
        return pmResenje;
    }

    public void setPmResenje(String pmResenje) {
        this.pmResenje = pmResenje;
    }

    @Basic
    @Column(name = "Lek_Id", nullable = true)
    public int getLekId() {
        return lekId;
    }

    public void setLekId(int lekId) {
        this.lekId = lekId;
    }

    @Basic
    @Column(name = "Materijal", nullable = false, length = 60)
    public String getMaterijal() {
        return materijal;
    }

    public void setMaterijal(String materijal) {
        this.materijal = materijal;
    }

    @Basic
    @Column(name = "Namena_promocije", nullable = true, length = 20)
    public String getNamenaPromocije() {
        return namenaPromocije;
    }

    public void setNamenaPromocije(String namenaPromocije) {
        this.namenaPromocije = namenaPromocije;
    }

    @Basic
    @Column(name = "Vazi_od", nullable = true)
    public Date getVaziOd() {
        return vaziOd;
    }

    public void setVaziOd(Date vaziOd) {
        this.vaziOd = vaziOd;
    }

    @Basic
    @Column(name = "Vazi_do", nullable = true)
    public Date getVaziDo() {
        return vaziDo;
    }

    public void setVaziDo(Date vaziDo) {
        this.vaziDo = vaziDo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromotivniMaterijal that = (PromotivniMaterijal) o;

        if (pmResenje != null ? !pmResenje.equals(that.pmResenje) : that.pmResenje != null) return false;
        if (materijal != null ? !materijal.equals(that.materijal) : that.materijal != null) return false;
        if (namenaPromocije != null ? !namenaPromocije.equals(that.namenaPromocije) : that.namenaPromocije != null)
            return false;
        if (vaziOd != null ? !vaziOd.equals(that.vaziOd) : that.vaziOd != null) return false;
        if (vaziDo != null ? !vaziDo.equals(that.vaziDo) : that.vaziDo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = pmResenje != null ? pmResenje.hashCode() : 0;
        result = 31 * result + (materijal != null ? materijal.hashCode() : 0);
        result = 31 * result + (namenaPromocije != null ? namenaPromocije.hashCode() : 0);
        result = 31 * result + (vaziOd != null ? vaziOd.hashCode() : 0);
        result = 31 * result + (vaziDo != null ? vaziDo.hashCode() : 0);
        return result;
    }
}

package org.raf.si.zdravstvo.limes.dao;

import org.raf.si.zdravstvo.limes.dao.repositories.ClientRepository;
import org.raf.si.zdravstvo.limes.dao.repositories.Crud;
import org.raf.si.zdravstvo.limes.dto.ClientDto;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.keys.KlijentPK;
import org.raf.si.zdravstvo.limes.entites.many2many.Klijent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ClientDao implements Crud<Klijent, KlijentPK> {

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    CompanyDao companyDao;

    @Autowired
    MedicalTestingDao medicalTestingDao;

    @Override
    public Iterable<Klijent> getAll() {
        return clientRepository.findAll();
    }

    public List<ClientDto> smartGetAll() {
        List<ClientDto> smartClients = new ArrayList<>();
        Iterable<Klijent> clients = getAll();
        String companyName, testName;
        for(Klijent k : clients) {
            companyName = companyDao.getById(k.getKompanijaId()).getKompanijaNaziv();
            testName = medicalTestingDao.getById(k.getiResenje()).getiNaziv();
            smartClients.add(new ClientDto(companyName, testName));
        }
        return smartClients;
    }

    @Override
    public Klijent getById(KlijentPK id) {
        return clientRepository.findOne(id);
    }

    public ClientDto smartGetById(KlijentPK id) {
        Klijent k = getById(id);
        String companyName = companyDao.getById(k.getKompanijaId()).getKompanijaNaziv();
        String testName = medicalTestingDao.getById(k.getiResenje()).getiNaziv();
        return new ClientDto(companyName, testName);
    }

    @Override
    public void add(Klijent klijent) {
        clientRepository.save(klijent);
    }

    @Override
    public void delete(KlijentPK id) {
        clientRepository.delete(id);
    }

    @Override
    public Iterable<Klijent> paramSearch(ParamsDto paramsDto) {
        return null;
    }
}

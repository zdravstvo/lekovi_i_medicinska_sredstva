package org.raf.si.zdravstvo.limes.entites.keys;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class DozvolePK implements Serializable {
    private int lekId;
    private int komKompanijaId;
    private int kompanijaId;

    @Column(name = "Lek_Id", nullable = false)
    @Id
    public int getLekId() {
        return lekId;
    }

    public void setLekId(int lekId) {
        this.lekId = lekId;
    }

    @Column(name = "Kom_Kompanija_Id", nullable = false)
    @Id
    public int getKomKompanijaId() {
        return komKompanijaId;
    }

    public void setKomKompanijaId(int komKompanijaId) {
        this.komKompanijaId = komKompanijaId;
    }

    @Column(name = "Kompanija_Id", nullable = false)
    @Id
    public int getKompanijaId() {
        return kompanijaId;
    }

    public void setKompanijaId(int kompanijaId) {
        this.kompanijaId = kompanijaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DozvolePK that = (DozvolePK) o;

        if (lekId != that.lekId) return false;
        if (komKompanijaId != that.komKompanijaId) return false;
        if (kompanijaId != that.kompanijaId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = lekId;
        result = 31 * result + komKompanijaId;
        result = 31 * result + kompanijaId;
        return result;
    }
}

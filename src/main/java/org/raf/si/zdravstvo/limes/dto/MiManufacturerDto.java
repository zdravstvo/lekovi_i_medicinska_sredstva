package org.raf.si.zdravstvo.limes.dto;

public class MiManufacturerDto {

    private String companyName;
    private String medicalInstrumentName;

    public MiManufacturerDto() {
    }

    public MiManufacturerDto(String companyName, String medicalInstrumentName) {
        this.companyName = companyName;
        this.medicalInstrumentName = medicalInstrumentName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getMedicalInstrumentName() {
        return medicalInstrumentName;
    }

    public void setMedicalInstrumentName(String medicalInstrumentName) {
        this.medicalInstrumentName = medicalInstrumentName;
    }
}

package org.raf.si.zdravstvo.limes.dao.repositories;

import org.raf.si.zdravstvo.limes.entites.PromotivniMaterijal;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface PromotionalMaterialRepository extends CrudRepository<PromotivniMaterijal, String> {

    public Iterable<PromotivniMaterijal> findByPmResenje(String param);

    public Iterable<PromotivniMaterijal> findByMaterijal(int param);

    public Iterable<PromotivniMaterijal> findByNamenaPromocije(String param);

    public Iterable<PromotivniMaterijal> findByVaziDo(Date param);

    public Iterable<PromotivniMaterijal> findByVaziOd(Date param);

}

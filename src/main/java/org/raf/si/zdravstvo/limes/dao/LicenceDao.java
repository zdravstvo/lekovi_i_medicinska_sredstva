package org.raf.si.zdravstvo.limes.dao;

import org.raf.si.zdravstvo.limes.dao.repositories.Crud;
import org.raf.si.zdravstvo.limes.dao.repositories.LicenceRepository;
import org.raf.si.zdravstvo.limes.dto.LicenceDto;
import org.raf.si.zdravstvo.limes.dto.LicenceReportDto;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.dto.PeriodDto;
import org.raf.si.zdravstvo.limes.entites.Dozvole;
import org.raf.si.zdravstvo.limes.entites.keys.DozvolePK;
import org.raf.si.zdravstvo.limes.entites.keys.LekoviProizvodjacPK;
import org.raf.si.zdravstvo.limes.entites.many2many.LekoviProizvodjac;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class LicenceDao implements Crud<Dozvole, DozvolePK> {

    @Autowired
    LicenceRepository licenceRepository;

    @Autowired
    CompanyDao companyDao;

    @Autowired
    MedicineDao medicineDao;

    @Override
    public Iterable<Dozvole> getAll() {
        return licenceRepository.findAll();
    }

    public List<LicenceDto> smartGetAll() {
        List<LicenceDto> smartLicences = new ArrayList<>();
        Iterable<Dozvole> clients = getAll();
        String companyName, komCompanyName, medicineName;
        for(Dozvole k : clients) {
            companyName = companyDao.getById(k.getKompanijaId()).getKompanijaNaziv();
            komCompanyName = companyDao.getById(k.getKomKompanijaId()).getKompanijaNaziv();
            medicineName = medicineDao.getById(k.getLekId()).getLekNaziv();
            smartLicences.add(new LicenceDto(companyName, komCompanyName, medicineName));
        }
        return smartLicences;
    }

    @Override
    public Dozvole getById(DozvolePK id) {
        return licenceRepository.findOne(id);
    }

    public LicenceDto smartGetById(DozvolePK id) {
        Dozvole k = getById(id);
        String companyName = companyDao.getById(k.getKompanijaId()).getKompanijaNaziv();
        String komCompanyName = companyDao.getById(k.getKomKompanijaId()).getKompanijaNaziv();
        String medicineName = medicineDao.getById(k.getLekId()).getLekNaziv();
        return new LicenceDto(companyName, komCompanyName, medicineName);
    }

    @Override
    public void add(Dozvole dozvola) {
        licenceRepository.save(dozvola);
    }

    @Override
    public void delete(DozvolePK id) {
        licenceRepository.delete(id);
    }

    @Override
    public Iterable<Dozvole> paramSearch(ParamsDto paramsDto) {
        return null;
    }

    public List<LicenceReportDto> getReport(PeriodDto periodDto) {
        HashMap<String, Integer> report = new HashMap<>();
        Iterable<Dozvole> licences = licenceRepository.findByDatumBetween(periodDto.getFromDate(), periodDto.getToDate());
        List<LicenceReportDto> reportDtoList = new ArrayList<>();

        String medicineName;
        for(Dozvole k : licences) {
            medicineName = medicineDao.getById(k.getLekId()).getLekNaziv();
            if (report.containsKey(medicineName)) {
                int amount = report.get(medicineName);
                report.put(medicineName, amount + k.getKolicina());
            }
            else {
                report.put(medicineName, k.getKolicina());
            }
        }

        for(String key: report.keySet()) {
            reportDtoList.add(new LicenceReportDto(key, report.get(key)));
        }

        return reportDtoList;
    }


}

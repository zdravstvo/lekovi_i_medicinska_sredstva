package org.raf.si.zdravstvo.limes.dao.repositories;

import org.raf.si.zdravstvo.limes.entites.keys.KlijentPK;
import org.raf.si.zdravstvo.limes.entites.many2many.Klijent;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Klijent, KlijentPK> {

}

package org.raf.si.zdravstvo.limes.dao.repositories;

import org.raf.si.zdravstvo.limes.entites.keys.NosilacPK;
import org.raf.si.zdravstvo.limes.entites.many2many.Nosilac;
import org.springframework.data.repository.CrudRepository;

public interface CarrierRepository extends CrudRepository<Nosilac, NosilacPK> {
}

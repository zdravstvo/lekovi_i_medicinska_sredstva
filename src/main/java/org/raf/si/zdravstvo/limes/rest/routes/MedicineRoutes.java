package org.raf.si.zdravstvo.limes.rest.routes;

import org.apache.camel.builder.RouteBuilder;
import org.raf.si.zdravstvo.limes.dao.MedicineDao;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.Lekovi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MedicineRoutes extends RouteBuilder {

    @Autowired
    MedicineDao medicineDao;

    @Override
    public void configure() throws Exception {

        rest("/medicine").description("Medicine Rest service")

                .get("/getAll").produces("application/json").description("The list of all medicine")
                    .route().id("medicineRoute")
                    .bean(medicineDao, "getAll")
                    .endRest()

                .delete("/{id}").description("Delete the medicine by a unique identifier")
                    .route().id("medicineDeleteRoute")
                    .bean(medicineDao, "delete(${headers.id})")
                    .endRest()

                .post("/params").type(ParamsDto.class).produces("application/json").description("Get all medicine that fulfill the criteria")
                    .route().id("medicineParamsRoute")
                    .bean(medicineDao, "paramSearch(${body})")
                    .endRest()

                .post("/insert").type(Lekovi.class).description("Add new medicine to the database")
                    .route().id("medicineAddRoute")
                    .bean(medicineDao, "add(${body})")
                    .endRest();



    }
}

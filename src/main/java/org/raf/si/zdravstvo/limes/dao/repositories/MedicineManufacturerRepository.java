package org.raf.si.zdravstvo.limes.dao.repositories;

import org.raf.si.zdravstvo.limes.entites.keys.LekoviProizvodjacPK;
import org.raf.si.zdravstvo.limes.entites.many2many.LekoviProizvodjac;
import org.springframework.data.repository.CrudRepository;

public interface MedicineManufacturerRepository extends CrudRepository<LekoviProizvodjac, LekoviProizvodjacPK> {
}

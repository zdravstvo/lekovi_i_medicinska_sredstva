package org.raf.si.zdravstvo.limes.dao.repositories;

import org.raf.si.zdravstvo.limes.entites.Ispitivanje;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface MedicalTestingRepository extends CrudRepository<Ispitivanje, String> {

    public Iterable<Ispitivanje> findByIResenje(String param);

    public Iterable<Ispitivanje> findByINaziv(String param);

    public Iterable<Ispitivanje> findByOpis(String param);

    public Iterable<Ispitivanje> findByBrojProtokola(String param);

    public Iterable<Ispitivanje> findByIIzdato(Date param);

    public Iterable<Ispitivanje> findByStatus(String param);

}

package org.raf.si.zdravstvo.limes.dto;

public class LicenceDto {

    private String companyName;
    private String komCompanyName;
    private String medicineName;

    public LicenceDto() {
    }

    public LicenceDto(String companyName, String komCompanyName, String medicineName) {
        this.companyName = companyName;
        this.komCompanyName = komCompanyName;
        this.medicineName = medicineName;
    }

    public String getCompanyName() { return companyName; }

    public void setCompanyName(String companyName) { this.companyName = companyName; }

    public String getKomCompanyName() { return komCompanyName; }

    public void setKomCompanyName(String companyName) { this.komCompanyName = companyName; }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

}

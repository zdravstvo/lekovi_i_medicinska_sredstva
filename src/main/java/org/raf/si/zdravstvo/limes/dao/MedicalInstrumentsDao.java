package org.raf.si.zdravstvo.limes.dao;

import org.apache.commons.lang3.StringUtils;
import org.raf.si.zdravstvo.limes.dao.repositories.Crud;
import org.raf.si.zdravstvo.limes.dao.repositories.MedicalInstrumentRepository;
import org.raf.si.zdravstvo.limes.dao.repositories.MedicineRepository;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.Lekovi;
import org.raf.si.zdravstvo.limes.entites.MedicinskaSredstva;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class MedicalInstrumentsDao implements Crud<MedicinskaSredstva, Integer> {

    @Autowired
    MedicalInstrumentRepository medicalInstrumentRepository;

    @Override
    public Iterable<MedicinskaSredstva> getAll() {
        return medicalInstrumentRepository.findAll();
    }

    @Override
    public MedicinskaSredstva getById(Integer id) {
        return medicalInstrumentRepository.findOne(id);
    }

    @Override
    public void add(MedicinskaSredstva medicalInstrument) {
        medicalInstrumentRepository.save(medicalInstrument);
    }

    @Override
    public void delete(Integer id) {
        medicalInstrumentRepository.delete(id);
    }

    @Override
    public Iterable<MedicinskaSredstva> paramSearch(ParamsDto paramsDto) {
        Set<String> keys = paramsDto.getParameterMap().keySet();
        Collection<Object> values = paramsDto.getParameterMap().values();
        String value = (String) values.toArray()[0];
        String key = (String) keys.toArray()[0];
        if (StringUtils.containsIgnoreCase(key, "id")) {
            List<MedicinskaSredstva> iterable = new ArrayList<>();
            iterable.add(medicalInstrumentRepository.findOne(Integer.parseInt(value)));
            return iterable;
        }
        Class ca = MedicalInstrumentRepository.class;
        Method[] methods = ca.getDeclaredMethods();
        Object[] params = new Object[1];
        if (StringUtils.containsIgnoreCase(key, "izdato") || StringUtils.containsIgnoreCase(key, "istice")) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date date = sdf.parse(value);
                params[0] = date;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            params[0] = value;
        }
        for (Method method : methods) {
            if (StringUtils.containsIgnoreCase(method.getName(), key)) {
                method.setAccessible(true);
                try {
                    return (Iterable<MedicinskaSredstva>) method.invoke(medicalInstrumentRepository, params);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}

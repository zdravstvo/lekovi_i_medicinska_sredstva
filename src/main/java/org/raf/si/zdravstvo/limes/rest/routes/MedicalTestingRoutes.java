package org.raf.si.zdravstvo.limes.rest.routes;

import org.apache.camel.builder.RouteBuilder;
import org.raf.si.zdravstvo.limes.dao.MedicalTestingDao;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.Ispitivanje;
import org.raf.si.zdravstvo.limes.entites.Lekovi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MedicalTestingRoutes extends RouteBuilder {

    @Autowired
    MedicalTestingDao medicalTestingDao;

    @Override
    public void configure() throws Exception {

        rest("/medical-testing").description("Medical test Rest service")

                .get("/getAll").produces("application/json").description("The list of all medical tests")
                    .route().id("medicalTestRoute")
                    .bean(medicalTestingDao, "getAll")
                    .endRest()

                .delete("/{id}").description("Delete the medical test by a unique identifier")
                    .route().id("medicalTestDeleteRoute")
                    .bean(medicalTestingDao, "delete(${headers.id})")
                    .endRest()

                .post("/params").type(ParamsDto.class).produces("application/json").description("Get all medical tests that fulfill the criteria")
                    .route().id("medicalTestParamsRoute")
                    .bean(medicalTestingDao, "paramSearch(${body})")
                    .endRest()

                .post("/insert").type(Ispitivanje.class).description("Add new medical test to the database")
                    .route().id("medicalTestAddRoute")
                    .bean(medicalTestingDao, "add(${body})")
                    .endRest();

    }
}

package org.raf.si.zdravstvo.limes.dao.repositories;

import org.raf.si.zdravstvo.limes.entites.MedicinskaSredstva;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface MedicalInstrumentRepository extends CrudRepository<MedicinskaSredstva, Integer> {

    Iterable<MedicinskaSredstva> findByGenerickiNaziv(String param);

    Iterable<MedicinskaSredstva> findByMsNaziv(String param);

    Iterable<MedicinskaSredstva> findByKlasa(String param);

    Iterable<MedicinskaSredstva> findByKategorija(String param);

    Iterable<MedicinskaSredstva> findByMestoIzdavanja(String param);

    Iterable<MedicinskaSredstva> findByMsResenje(String param);

    Iterable<MedicinskaSredstva> findByResenjeIstice(Date param);

    Iterable<MedicinskaSredstva> findByResenjeIzdato(Date param);
}

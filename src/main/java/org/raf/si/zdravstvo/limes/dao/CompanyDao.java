package org.raf.si.zdravstvo.limes.dao;

import org.apache.commons.lang3.StringUtils;
import org.raf.si.zdravstvo.limes.dao.repositories.CompanyRepository;
import org.raf.si.zdravstvo.limes.dao.repositories.Crud;
import org.raf.si.zdravstvo.limes.dao.repositories.MedicalInstrumentRepository;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.Kompanija;
import org.raf.si.zdravstvo.limes.entites.MedicinskaSredstva;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Component
public class CompanyDao implements Crud<Kompanija, Integer> {

    @Autowired
    CompanyRepository companyRepository;

    @Override
    public Iterable<Kompanija> getAll() {
        return companyRepository.findAll();
    }

    @Override
    public Kompanija getById(Integer id) {
        return companyRepository.findOne(id);
    }

    @Override
    public void add(Kompanija company) {
        companyRepository.save(company);
    }

    @Override
    public void delete(Integer id) {
        companyRepository.delete(id);
    }

    @Override
    public Iterable<Kompanija> paramSearch(ParamsDto paramsDto) {
        Set<String> keys = paramsDto.getParameterMap().keySet();
        Collection<Object> values = paramsDto.getParameterMap().values();
        String value = (String) values.toArray()[0];
        String key = (String) keys.toArray()[0];
        if (StringUtils.containsIgnoreCase(key, "id")) {
            List<Kompanija> iterable = new ArrayList<>();
            iterable.add(companyRepository.findOne(Integer.parseInt(value)));
            return iterable;
        }
        Class ca = CompanyRepository.class;
        Method[] methods = ca.getDeclaredMethods();
        Object[] params = new Object[1];
        params[0] = value;
        for (Method method : methods) {
            if (StringUtils.containsIgnoreCase(method.getName(), key)) {
                method.setAccessible(true);
                try {
                    return (Iterable<Kompanija>) method.invoke(companyRepository, params);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}

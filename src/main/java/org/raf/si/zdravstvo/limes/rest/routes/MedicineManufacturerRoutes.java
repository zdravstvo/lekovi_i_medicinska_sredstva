package org.raf.si.zdravstvo.limes.rest.routes;

import org.apache.camel.builder.RouteBuilder;
import org.raf.si.zdravstvo.limes.dao.MedicineManufacturerDao;
import org.raf.si.zdravstvo.limes.dto.MedicineManufacturerDto;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.keys.LekoviProizvodjacPK;
import org.raf.si.zdravstvo.limes.entites.many2many.LekoviProizvodjac;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MedicineManufacturerRoutes extends RouteBuilder {

    @Autowired
    MedicineManufacturerDao medicineManufacturerDao;

    @Override
    public void configure() throws Exception {

        rest("/medicine-manufacturers").description("Medicine manufacturer Rest service")

                .get("/getAll").produces("application/json").description("The list of all medicine manufacturers")
                    .route().id("medicineManufacturerRoute")
                    .bean(medicineManufacturerDao, "smartGetAll")
                    .endRest()

                .post("/id").type(LekoviProizvodjacPK.class).outType(MedicineManufacturerDto.class).description("Get the medicine manufacturer by a unique identifier")
                    .route().id("medicineManufacturerIdRoute")
                    .bean(medicineManufacturerDao, "smartGetById(${body})")
                    .endRest()

                .delete("/id").type(LekoviProizvodjac.class).description("Delete the medicine manufacturer by a unique identifier")
                    .route().id("medicineManufacturerDeleteRoute")
                    .bean(medicineManufacturerDao, "delete(${body})")
                    .endRest()

                .post("/params").type(ParamsDto.class).produces("application/json").description("Get all medicine manufacturers that fulfill the criteria")
                    .route().id("medicineManufacturerParamsRoute")
                    .bean(medicineManufacturerDao, "paramSearch(${body})")
                    .endRest()

                .post("/insert").type(LekoviProizvodjac.class).description("Add new medicine manufacturer to the database")
                    .route().id("medicineManufacturerAddRoute")
                    .bean(medicineManufacturerDao, "add(${body})")
                    .endRest();

    }

}

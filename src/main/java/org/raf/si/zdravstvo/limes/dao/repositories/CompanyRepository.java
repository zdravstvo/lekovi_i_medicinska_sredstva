package org.raf.si.zdravstvo.limes.dao.repositories;

import org.raf.si.zdravstvo.limes.entites.Kompanija;
import org.springframework.data.repository.CrudRepository;

public interface CompanyRepository extends CrudRepository<Kompanija, Integer> {

    public Iterable<Kompanija> findByKompanijaNaziv(String param);

    public Iterable<Kompanija> findByTip(String param);

    public Iterable<Kompanija> findByUlica(String param);

    public Iterable<Kompanija> findByGrad(String param);

    public Iterable<Kompanija> findByDrzava(String param);

}

package org.raf.si.zdravstvo.limes.entites.keys;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class KlijentPK implements Serializable {
    private int kompanijaId;
    private String iResenje;

    @Column(name = "Kompanija_Id", nullable = false)
    @Id
    public int getKompanijaId() {
        return kompanijaId;
    }

    public void setKompanijaId(int kompanijaId) {
        this.kompanijaId = kompanijaId;
    }

    @Column(name = "I_resenje", nullable = false, length = 50)
    @Id
    public String getiResenje() {
        return iResenje;
    }

    public void setiResenje(String iResenje) {
        this.iResenje = iResenje;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KlijentPK that = (KlijentPK) o;

        if (kompanijaId != that.kompanijaId) return false;
        if (iResenje != null ? !iResenje.equals(that.iResenje) : that.iResenje != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = kompanijaId;
        result = 31 * result + (iResenje != null ? iResenje.hashCode() : 0);
        return result;
    }
}

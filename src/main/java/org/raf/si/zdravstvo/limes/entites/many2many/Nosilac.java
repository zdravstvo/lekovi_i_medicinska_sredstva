package org.raf.si.zdravstvo.limes.entites.many2many;

import org.raf.si.zdravstvo.limes.entites.keys.NosilacPK;

import javax.persistence.*;

@Entity
@Table(name = "nosilac", schema = "limes", catalog = "")
@IdClass(NosilacPK.class)
public class Nosilac {
    private int kompanijaId;
    private String pmResenje;

    @Id
    @Column(name = "Kompanija_Id", nullable = false)
    public int getKompanijaId() {
        return kompanijaId;
    }

    public void setKompanijaId(int kompanijaId) {
        this.kompanijaId = kompanijaId;
    }

    @Id
    @Column(name = "PM_resenje", nullable = false, length = 50)
    public String getPmResenje() {
        return pmResenje;
    }

    public void setPmResenje(String pmResenje) {
        this.pmResenje = pmResenje;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Nosilac that = (Nosilac) o;

        if (kompanijaId != that.kompanijaId) return false;
        if (pmResenje != null ? !pmResenje.equals(that.pmResenje) : that.pmResenje != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = kompanijaId;
        result = 31 * result + (pmResenje != null ? pmResenje.hashCode() : 0);
        return result;
    }
}

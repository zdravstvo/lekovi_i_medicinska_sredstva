package org.raf.si.zdravstvo.limes.entites;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "medicinska_sredstva", schema = "limes", catalog = "")
public class MedicinskaSredstva {
    private int msId;
    private String msNaziv;
    private String generickiNaziv;
    private String klasa;
    private String kategorija;
    private String mestoIzdavanja;
    private String msResenje;
    private Date resenjeIzdato;
    private Date resenjeIstice;

    @Id
    @Column(name = "MS_Id", nullable = false)
    public int getMsId() {
        return msId;
    }

    public void setMsId(int msId) {
        this.msId = msId;
    }

    @Basic
    @Column(name = "MS_naziv", nullable = false, length = 40)
    public String getMsNaziv() {
        return msNaziv;
    }

    public void setMsNaziv(String msNaziv) {
        this.msNaziv = msNaziv;
    }

    @Basic
    @Column(name = "Genericki_naziv", nullable = true, length = 40)
    public String getGenerickiNaziv() {
        return generickiNaziv;
    }

    public void setGenerickiNaziv(String generickiNaziv) {
        this.generickiNaziv = generickiNaziv;
    }

    @Basic
    @Column(name = "Klasa", nullable = false, length = 40)
    public String getKlasa() {
        return klasa;
    }

    public void setKlasa(String klasa) {
        this.klasa = klasa;
    }

    @Basic
    @Column(name = "Kategorija", nullable = true, length = 40)
    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    @Basic
    @Column(name = "Mesto_izdavanja", nullable = true, length = 30)
    public String getMestoIzdavanja() {
        return mestoIzdavanja;
    }

    public void setMestoIzdavanja(String mestoIzdavanja) {
        this.mestoIzdavanja = mestoIzdavanja;
    }

    @Basic
    @Column(name = "MS_resenje", nullable = true, length = 50)
    public String getMsResenje() {
        return msResenje;
    }

    public void setMsResenje(String msResenje) {
        this.msResenje = msResenje;
    }

    @Basic
    @Column(name = "Resenje_izdato", nullable = true)
    public Date getResenjeIzdato() {
        return resenjeIzdato;
    }

    public void setResenjeIzdato(Date resenjeIzdato) {
        this.resenjeIzdato = resenjeIzdato;
    }

    @Basic
    @Column(name = "Resenje_istice", nullable = true)
    public Date getResenjeIstice() {
        return resenjeIstice;
    }

    public void setResenjeIstice(Date resenjeIstice) {
        this.resenjeIstice = resenjeIstice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MedicinskaSredstva that = (MedicinskaSredstva) o;

        if (msId != that.msId) return false;
        if (msNaziv != null ? !msNaziv.equals(that.msNaziv) : that.msNaziv != null) return false;
        if (generickiNaziv != null ? !generickiNaziv.equals(that.generickiNaziv) : that.generickiNaziv != null)
            return false;
        if (klasa != null ? !klasa.equals(that.klasa) : that.klasa != null) return false;
        if (kategorija != null ? !kategorija.equals(that.kategorija) : that.kategorija != null) return false;
        if (mestoIzdavanja != null ? !mestoIzdavanja.equals(that.mestoIzdavanja) : that.mestoIzdavanja != null)
            return false;
        if (msResenje != null ? !msResenje.equals(that.msResenje) : that.msResenje != null) return false;
        if (resenjeIzdato != null ? !resenjeIzdato.equals(that.resenjeIzdato) : that.resenjeIzdato != null)
            return false;
        if (resenjeIstice != null ? !resenjeIstice.equals(that.resenjeIstice) : that.resenjeIstice != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = msId;
        result = 31 * result + (msNaziv != null ? msNaziv.hashCode() : 0);
        result = 31 * result + (generickiNaziv != null ? generickiNaziv.hashCode() : 0);
        result = 31 * result + (klasa != null ? klasa.hashCode() : 0);
        result = 31 * result + (kategorija != null ? kategorija.hashCode() : 0);
        result = 31 * result + (mestoIzdavanja != null ? mestoIzdavanja.hashCode() : 0);
        result = 31 * result + (msResenje != null ? msResenje.hashCode() : 0);
        result = 31 * result + (resenjeIzdato != null ? resenjeIzdato.hashCode() : 0);
        result = 31 * result + (resenjeIstice != null ? resenjeIstice.hashCode() : 0);
        return result;
    }
}

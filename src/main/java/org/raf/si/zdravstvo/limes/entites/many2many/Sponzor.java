package org.raf.si.zdravstvo.limes.entites.many2many;

import org.raf.si.zdravstvo.limes.entites.keys.SponzorPK;

import javax.persistence.*;

@Entity
@Table(name = "sponzor", schema = "limes", catalog = "")
@IdClass(SponzorPK.class)
public class Sponzor {
    private int kompanijaId;
    private String iResenje;

    @Id
    @Column(name = "Kompanija_Id", nullable = false)
    public int getKompanijaId() {
        return kompanijaId;
    }

    public void setKompanijaId(int kompanijaId) {
        this.kompanijaId = kompanijaId;
    }

    @Id
    @Column(name = "I_resenje", nullable = false, length = 50)
    public String getiResenje() {
        return iResenje;
    }

    public void setiResenje(String iResenje) {
        this.iResenje = iResenje;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sponzor that = (Sponzor) o;

        if (kompanijaId != that.kompanijaId) return false;
        if (iResenje != null ? !iResenje.equals(that.iResenje) : that.iResenje != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = kompanijaId;
        result = 31 * result + (iResenje != null ? iResenje.hashCode() : 0);
        return result;
    }
}

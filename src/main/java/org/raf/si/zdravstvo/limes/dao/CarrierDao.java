package org.raf.si.zdravstvo.limes.dao;

import org.raf.si.zdravstvo.limes.dao.repositories.CarrierRepository;
import org.raf.si.zdravstvo.limes.dao.repositories.Crud;
import org.raf.si.zdravstvo.limes.dto.CarrierDto;
import org.raf.si.zdravstvo.limes.dto.ParamsDto;
import org.raf.si.zdravstvo.limes.entites.keys.NosilacPK;
import org.raf.si.zdravstvo.limes.entites.many2many.Nosilac;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CarrierDao implements Crud<Nosilac, NosilacPK> {

    @Autowired
    CarrierRepository carrierRepository;

    @Autowired
    CompanyDao companyDao;

    @Autowired
    PromotionalMaterialDao promotionalMaterialDao;

    @Override
    public Iterable<Nosilac> getAll() {
        return carrierRepository.findAll();
    }

    public List<CarrierDto> smartGetAll() {
        List<CarrierDto> smartManufacturers = new ArrayList<>();
        Iterable<Nosilac> clients = getAll();
        String companyName, solution;
        for(Nosilac k : clients) {
            companyName = companyDao.getById(k.getKompanijaId()).getKompanijaNaziv();
            solution = promotionalMaterialDao.getById(k.getPmResenje()).getPmResenje();
            smartManufacturers.add(new CarrierDto(companyName, solution));
        }
        return smartManufacturers;
    }

    @Override
    public Nosilac getById(NosilacPK id) {
        return carrierRepository.findOne(id);
    }

    public CarrierDto smartGetById(NosilacPK id) {
        Nosilac k = getById(id);
        String companyName = companyDao.getById(k.getKompanijaId()).getKompanijaNaziv();
        String solution = promotionalMaterialDao.getById(k.getPmResenje()).getPmResenje();
        return new CarrierDto(companyName, solution);
    }

    @Override
    public void add(Nosilac nosilac) {
        carrierRepository.save(nosilac);
    }

    @Override
    public void delete(NosilacPK id) {
        carrierRepository.delete(id);
    }

    @Override
    public Iterable<Nosilac> paramSearch(ParamsDto paramsDto) {
        return null;
    }

}
